package mpbf

type Realm struct {
	ID   string
	Name string
}

type User struct {
	ID   string
	Name string
}

type Message struct {
	Content   string
	MessageID string

	Realm  *Realm
	Author *User
}
