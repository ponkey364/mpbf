# MPBF - Multi Platform Bot Framework
A framework for processing and handling commands in messages (and also for handling events) from various platforms.

## Info
License: MPL-2.0