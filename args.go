package mpbf

import (
	"strconv"
	"time"
)

type ArgType uint

const (
	ArgString ArgType = 1 << iota
	ArgInt
	ArgBool
	ArgTime
	ArgDuration
	ArgRealm
	ArgUser
	ArgRole
)

func DefaultArgParser(rawArgs []string, command *Command) ([]interface{}, error) {
	if command.Opts == nil || command.Opts.Args == nil {
		return nil, nil
	}

	if len(command.Opts.Args) == 0 {
		return []interface{}{}, nil
	}

	argTypes := command.Opts.Args
	typedArgs := []interface{}{}

	for i, argType := range argTypes {
		rawArg := rawArgs[i]

		var typedArg interface{}
		var err error

		switch argType {
		case ArgString, ArgRealm, ArgUser, ArgRole:
			typedArg = rawArg
		case ArgInt:
			typedArg, err = strconv.Atoi(rawArg)
		case ArgBool:
			typedArg, err = strconv.ParseBool(rawArg)
		case ArgTime:
			typedArg, err = time.Parse(time.RFC3339, rawArg)
		case ArgDuration:
			typedArg, err = time.ParseDuration(rawArg)
		}

		if err != nil {
			return nil, err
		}

		typedArgs = append(typedArgs, typedArg)
	}

	return typedArgs, nil
}
