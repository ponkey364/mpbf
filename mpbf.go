package mpbf

import (
	"fmt"
	"strings"

	"github.com/gammazero/radixtree"
)

// Bot Command & Config & other misc bits / bobs

type Middleware func(next CommandHandler) CommandHandler

type Config struct {
	Platforms  []Platform
	Middleware []Middleware
	Prefix     string
}

type BotCommander struct {
	platforms   map[string]Platform
	middleware  []Middleware
	Prefix      string
	CommandTrie *radixtree.Paths
}

func New(config *Config) *BotCommander {
	platformMap := map[string]Platform{}

	for _, platform := range config.Platforms {
		platformMap[platform.GetPlatformType()] = platform
	}

	return &BotCommander{
		platforms:   platformMap,
		middleware:  config.Middleware,
		Prefix:      config.Prefix,
		CommandTrie: radixtree.NewPaths("/"),
	}
}

func (bc *BotCommander) Start() error {
	for _, platform := range bc.platforms {
		if err := platform.Open(bc, bc.onMessage); err != nil {
			return err
		}
	}

	return nil
}

func (bc *BotCommander) Stop() error {
	var errr error

	for _, platform := range bc.platforms {
		if err := platform.Close(); err != nil {
			errr = err
		}
	}

	return errr
}

func (bc *BotCommander) GetPlatform(name string) Platform {
	if platform, ok := bc.platforms[name]; ok {
		return platform
	} else {
		return nil
	}
}

func (bc *BotCommander) Command(activator string, handler CommandHandler, opts *CommandOpts) *Command {
	cmd := &Command{
		Handler: handler,
		Opts:    opts,
	}

	bc.CommandTrie.Put(activator, cmd)
	return cmd
}

func (bc *BotCommander) Group(name string) *Group {
	return &Group{
		Name:        name,
		CommandTrie: bc.CommandTrie,
	}
}

func (bc *BotCommander) onMessage(req *Request, res Response) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from %s", r)
		}
	}()

	// is this a command?
	if !strings.HasPrefix(req.Content, bc.Prefix) {
		// well it doesn't start with our prefix, ignore it.
		return
	}
	req.Content = req.Content[len(bc.Prefix):]

	// split into a []string
	splitArgs := ArgsSplitter(req.Content)

	// get the activator
	activator := splitArgs[0]

	// does the activator exist in the trie?
	command, exists := bc.CommandTrie.Get(activator)

	if !exists {
		return
	}

	// do arg type funtimes
	platform := req.Context.Value("platform").(string)
	argParser := bc.platforms[platform].GetArgParser()
	typedArgs, err := argParser(splitArgs[1:], command.(*Command))
	if err != nil {
		panic(err)
	}

	req.Args = typedArgs

	// call the middleware chain + handler.
	cmdHandler := command.(*Command).Handler

	mws := bc.middleware
	if g := command.(*Command).group; g != nil {
		mws = append(mws, g.middleware...)
	}
	// this code adapted from https://github.com/go-chi/chi/blob/master/chain.go

	if len(mws) == 0 {
		cmdHandler(req, res)
		return
	}

	h := mws[len(mws)-1](cmdHandler)
	for i := len(mws) - 2; i >= 0; i-- {
		h = mws[i](h)
	}

	h(req, res)
}

// Groups & Commands

type Group struct {
	Name        string
	CommandTrie *radixtree.Paths

	middleware []Middleware
}

func (g *Group) Command(activator string, handler CommandHandler, opts *CommandOpts) *Command {
	cmd := &Command{
		Handler: handler,
		Opts:    opts,
		group:   g,
	}

	g.CommandTrie.Put(activator, cmd)
	return cmd
}

// Append some new middleware to *this group's* middleware stack
func (g *Group) Use(middlewares ...Middleware) *Group {
	g.middleware = append(g.middleware, middlewares...)

	return g
}

type Command struct {
	Handler CommandHandler
	Opts    *CommandOpts

	group *Group
}

type CommandHandler func(req *Request, res Response)

type CommandOpts struct {
	Name        string
	Description string
	Args        []ArgType
}

// Platform

type Platform interface {
	// this should be something like "discord" or "glimesh" to identify your platform. you should also export this as a const.
	GetPlatformType() string

	Open(bc *BotCommander, onMessage CommandHandler) error

	Close() error

	JoinRealm(realmID string) error

	LeaveRealm(realmID string) error

	// You can either return your own arg parser or return DefaultArgParser
	GetArgParser() func(rawArgs []string, command *Command) ([]interface{}, error)

	// Get the underlying connection to the platform (or an instance of a library you're using) (advanced use only)
	GetSession() interface{}
}
