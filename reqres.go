package mpbf

import "context"

type Request struct {
	Context context.Context

	// the types will be the types you specified in Command.Opts.Args
	Args []interface{}

	*Message
}

func (r *Request) WithContext(ctx context.Context) *Request {
	r.Context = ctx
	return r
}

type Response interface {
	Reply(message interface{}, to *Request) (*Message, error)

	Send(message interface{}, to string) (*Message, error)
}
